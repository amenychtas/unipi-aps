$(function () {
    $("button").click(function () {
        $("#entries").append("<p>A new entry</p>");
    });
    $("#entries")
        .on('mouseenter', 'p', function () {
            $(this).addClass('large_and_red');
        })
        .on('mouseleave', 'p', function () {
            $(this).removeClass('large_and_red');
        });
});
