'use strict';

const name = 'Josh Perez';
// const element = <h1>Hello, {name}</h1>;

const element = React.createElement(
    'h1',
    {},
    `Hello, ${name}`
);

const container = document.getElementById('root');
const root = ReactDOM.createRoot(container);
root.render(element);
