'use strict';

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};

        // this.stopClock = this.stopClock.bind(this);
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }

    stopClock() {
        clearInterval(this.timerID);
    }

    render() {
        return (
            <div>
                <h1>Hello, world!</h1>
                <h2>It is {this.state.date.toLocaleTimeString()}.</h2>
                <div>
                    <button onClick={() => {
                        this.stopClock()
                    }}>Stop</button>
                </div>
            </div>
        );
    }
}

const container = document.getElementById('root');
const root = ReactDOM.createRoot(container);
root.render(<Clock/>);
