const { onRequest } = require("firebase-functions/v2/https");
const { onDocumentCreated } = require("firebase-functions/v2/firestore");
const { onObjectFinalized } = require("firebase-functions/v2/storage");
const { getFirestore } = require('firebase-admin/firestore');

const { logger } = require("firebase-functions");

const { initializeApp } = require("firebase-admin/app");
initializeApp();

const cors = require('cors')({
    origin: true,
});

exports.helloWorld = onRequest(
    { region: "europe-west3" },
    (req, res) => {
        cors(req, res, () => {
            logger.info("Hello logs!", { structuredData: true });
            // response.send("Hello from Firebase!");
            res.send({
                first_name: "Andreas",
                last_name: "Menychtas",
                registered: true
            });
        });
    });

exports.onCreateUser = onDocumentCreated(
    {
        region: "europe-west3",
        document: 'users/{userId}'
    },
    (event) => {
        const original = event.data.data();

        logger.log("Uppercasing", event.params.documentId, original);

        const username = original.username.toUpperCase();
        return event.data.ref.set({
            username: username
        }, {
            merge: true
        });
    });

exports.onFileUpload = onObjectFinalized(
    {
        region: "europe-west3",
        bucket: "unipi-aps.appspot.com",
    },
    async (object) => {
        const filePath = object.name;
        const contentType = object.contentType;

        console.log("filePath: " + filePath);
        console.log("contentType " + contentType);

        // any other transformations here...

        return getFirestore()
            .collection('images')
            .doc()
            .set(object);
    });
